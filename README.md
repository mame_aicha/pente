# Pente

Simulates the game [Pente](https://www.ultraboardgames.com/pente/game-rules.php) and includes code to let various programs play together.

**NOTE:** This is a template with holes left for you to fill.
Running the program will throw exceptions telling you where you will find the holes that you should fill.

## Rules of the game

Pente is played on a 19x19 grid.

Player1 start with a center stone.

A player wins if they either align 5 stones (horizontally, vertically or diagonaly) or capture 5 pairs.
To capture a pair, you must place a stone on both side of two aligned stones owned by your adversary.

We *do not* use the Pro Pente rule that restricts the second move of the first player.

## Dependencies

- C++17, the flags have been set for g++
- [Boost.Asio](https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio.html) for the communications between programs (requires a recent Boost)

